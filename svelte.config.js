const path = require("path");
const sveltePreprocess = require("svelte-preprocess");

module.exports = {
	preprocess: sveltePreprocess({
		scss: {
			includePaths: [["src"], ["node_modules"], ["smui-theme"]].map(p =>
				path.join(...p)
			),
			sourceMap: true
		},
		pug: true,
		postcss: {
			plugins: [require("autoprefixer")]
		}
	})
};
